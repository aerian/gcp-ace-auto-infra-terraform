variable "region" {
	type  = string
	default = "us-east1"
}

variable "zone" {
	type  = string
	default = "us-east1-d"
}

variable "project_id" {
	type  = string
	default = "qwiklabs-gcp-01-09d393bac4f7"
}

variable "bucket_name" {
	type = string
	default = "tf-bucket-377368"
}

variable "bucket_location" {
	type = string
	default = "US"
}
