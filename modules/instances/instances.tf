resource "google_compute_instance" "tf-instance-1" {
  name         = "tf-instance-1"
  machine_type = var.machine_type
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.vm_boot_image
    }
  }

  network_interface {
    network = var.vpc_name
    subnetwork = "subnet-01"
  }

  metadata_startup_script = <<-EOT
        #!/bin/bash
  EOT
  allow_stopping_for_update = true
}

resource "google_compute_instance" "tf-instance-2" {
  name         = "tf-instance-2"
  machine_type = var.machine_type
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.vm_boot_image
    }
  }

  network_interface {
    network = var.vpc_name
    subnetwork = "subnet-02"
  }

  metadata_startup_script = <<-EOT
        #!/bin/bash
  EOT
  allow_stopping_for_update = true
}

/*
resource "google_compute_instance" "tf-instance-982981" {
  name         = "tf-instance-982981"
  machine_type = var.machine_type
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.vm_boot_image
    }
  }

  network_interface {
    network = "default"
  }

  metadata_startup_script = <<-EOT
        #!/bin/bash
  EOT
  allow_stopping_for_update = true
}
*/