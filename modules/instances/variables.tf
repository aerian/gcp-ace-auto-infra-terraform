variable "region" {
	type  = string
	default = "us-east1"
}

variable "zone" {
	type  = string
	default = "us-east1-d"
}

variable "project_id" {
	type  = string
	default = "qwiklabs-gcp-01-09d393bac4f7"
}

variable "machine_type" {
	type  = string
	default = "e2-standard-2"
}

variable "vm_boot_image" {
	type  = string
	default = "debian-11-bullseye-v20231010"
}

variable "vpc_name" {
  type    = string
  default = "tf-vpc-654004"
}