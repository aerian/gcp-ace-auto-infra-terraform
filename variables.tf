variable "region" {
  type    = string
  default = "us-east1"
}

variable "zone" {
  type    = string
  default = "us-east1-d"
}

variable "project_id" {
  type    = string
  default = "qwiklabs-gcp-01-09d393bac4f7"
}

variable "vpc_name" {
  type    = string
  default = "tf-vpc-654004"
}
